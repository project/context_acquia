<?php
/**
 * @file
 * Context condition plugin for Context Acquia.
 */

/**
 * Expose the current Acquia server environment as a context condition.
 */
class context_condition_acquia_environment extends context_condition {
  /**
   * Define allowed condition values.
   */
  function condition_values() {
    return array(
      CONTEXT_ACQUIA_LOCAL_KEY => t('Local development (non-Acquia)'),
      CONTEXT_ACQUIA_DEV_KEY => t('Acquia dev environment'),
      CONTEXT_ACQUIA_TEST_KEY => t('Acquia test environment'),
      CONTEXT_ACQUIA_LIVE_KEY => t('Acquia live environment'),
    );
  }

  /**
   * Execute.
   */
  function execute() {
    foreach ($this->get_contexts() as $context) {
      $environment_match = FALSE;
      foreach((array) $context->conditions['acquia_environment']['values'] as $key => $value) {
        // Local environment has no acquia server variable.
        if (empty($_ENV['AH_SITE_ENVIRONMENT'])) {
          if ($key === CONTEXT_ACQUIA_LOCAL_KEY) {
            $environment_match = TRUE;
          }
        }
        else {
          if ($key === $_ENV['AH_SITE_ENVIRONMENT']) {
            $environment_match = TRUE;
          }
        }
      }

      // If we have an environment match, let Context know.
      if ($environment_match) {
        $this->condition_met($context);
      }
    }
  }

}
